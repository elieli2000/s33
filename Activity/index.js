
//Fetch TodoList from jsonplaceholder
async function getToDoList() {


	const response = await fetch('https://jsonplaceholder.typicode.com/todos')
	const responseJson = await response.json();
	console.log(responseJson);
	const titles = responseJson.map((json)=> 'title: ' + json.title);
	console.log(titles);
}
getToDoList();

fetch('https://jsonplaceholder.typicode.com/todos/103')
.then((res)=>res.json())
.then((json)=>console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/103')
.then((res)=>res.json())
.then((json)=>{console.log(`title: ${json.title} | completed: ${json.completed}`);
});

//create TodoList from jsonplaceholder
async function createTodoList(){

	const response = await fetch('https://jsonplaceholder.typicode.com/todos',

	{
		method: "POST",
		headers:{
			"Content-Type": "application/json"
		},

		body:JSON.stringify({
			userId: 6,
			completed: true,
			title: "New Post"
		})
	});
const responseJson = await response.json();
console.log(responseJson);

}
createTodoList();

//update TodoList from jsonplaceholder

async function updateTodoList(){
		const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
{

	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "this is an updated to do"

	})
	
	});

	const responseJson = await  response.json();
	console.log(responseJson);
}
updateTodoList();


//update TodoList Structure from jsonplaceholder
async function updateTodoListStructure(){
const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
{
method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "this is a restructured to do",
		description: "new description",
		status: "completed",
		date_completed: '2022-08-27',
		userId: 314231

	})

});

const responseJson = await response.json();
console.log(responseJson);

}
updateTodoListStructure()

//use patch 
async function patchTodoList (){
	const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
	{

		method: "PATCH",
		headers: {
		"Content-Type": "application/json"

	},
	body:JSON.stringify({
		title: "this is a patched to do",
	})

});
const responseJson = await response.json();
console.log(responseJson);

}

patchTodoList();

//Display Completed ToDoList
async function completedTodoList (){
	const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
	{

		method: "PATCH",
		headers: {
		"Content-Type": "application/json"

	},
	body:JSON.stringify({
		completed: true,
		date_completed: '2022-08-26'
	})

});
const responseJson = await response.json();
console.log(responseJson);

}
completedTodoList();

//Delete

async function deleteTodoList (){
	const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
	{

		method: "DELETE"
		
	});


const responseJson = await response.json();
console.log(responseJson);
console.log('Method successfully deleted');

}
deleteTodoList();
